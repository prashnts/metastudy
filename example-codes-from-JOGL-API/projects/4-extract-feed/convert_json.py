#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-

import os
import sys
import pandas as pd

path = 'feeds'

# r=root, d=directories, f = files
for r, d, f in os.walk(path):
    for file in f:
        if '.json' in file and not 'tsv' in file:
            file_f = os.path.join(r, file)
            table = pd.read_json(file_f, encoding='utf8')  

            if table.empty:
                continue

            posters = [ t['id'] for t in table.creator ]
            posters_n = [ 'poster' for p in posters ]

            comments = table.comments

            commenters =  [ t['creator']['id'] for c in comments for t in c ] 
            commenters_n =  [ 'commenter' for c in comments for t in c ] 


            df = pd.DataFrame({
                'Id': posters,
                'Type': posters_n})

            df = df.append(pd.DataFrame({
                'Id': commenters,
                'Type': commenters_n}))

            df.to_csv(file_f+'.tsv', sep='\t', index=False)
